function chart(options) {
    var obj=Object();
    obj.opt=Object.assign({
        el:"chart",//canvas id
        num:5,//图形边数量
        //图形数据
        data:[
            {
                name:'可爱',//名称
                per:'60%'//所占百分比
            },
            {
                name:'天使',
                per:'40%'
            },
            {
                name:'萌萌值',
                per:'80%'
            },
            {
                name:'朴实值',
                per:'90%'
            },
            {
                name:'明星像',
                per:'100%'
            }
        ],
        length:'200',//图形边长
        part:3,//图形内部等级
        color:["white"],//图形内部等级颜色 从外到内
        line_color:'black',//边框颜色
        line_width:1,//边框宽度
        chart_global_alpha:0.2,//数据区域背景透明度
        chart_bg_color:'blue',//数据区域背景颜色
        chart_line_color:'black',//数据区域边框颜色
        chart_line_width:2,//数据区域边框宽度
        chart_title_font:"18px Arial"//数据区域标题字体

    },options);
    obj.can=null;//保存canvas对象
    obj.wid=0;//canvas 宽度
    obj.hei=0;//canvas 高度
    //中心点坐标
    obj.centerPosition={
    };
    //其他点坐标
    obj.pointPosition=[

    ];
    //等级点坐标
    obj.gradePosition=[
    ];
    obj.djxLen=0;//中心点到角的长度
    //获取中心点坐标
    obj.getCenter=function () {
        this.wid=this.can.width;
        this.hei=this.can.height;
        return this.centerPosition={
            x:parseInt(this.wid/2),
            y:parseInt(this.hei/2)
        };
    };
    //获取中心点到角的长度
    obj.getLen=function () {
        return this.djxLen=parseInt((this.opt.length/2)/(Math.sin(Math.PI/this.opt.num)));

    };
    //获取其他点坐标
    obj.getOtherP=function () {
        var pointData=[];//计算图形其他点的坐标
        var data=this.opt.data;//计算数据区域其他点的坐标
        var gradeData=[];
        for(var i=0;i<this.opt.num;i++)
        {
            var o=(2*Math.PI*i)/this.opt.num;
            var opx=Math.sin(o)*this.djxLen;
            var opy=Math.cos(o)*this.djxLen;
            var per=Number(data[i].per.replace('%',''));
            data[i].x=this.centerPosition.x+(per*opx/100);
            data[i].y=this.centerPosition.y-(per*opy/100);

            var title_font=this.opt.chart_title_font;
            var a=/([0-9]+)px/;
            var font_size=parseInt(a.exec(title_font)[1]);
            pointData[i]={
                x:parseInt(this.centerPosition.x+opx),
                y:parseInt(this.centerPosition.y-opy),
                fontx:parseInt(this.centerPosition.x+opx-0.5*data[i].name.length*font_size),
                fonty:parseInt(this.centerPosition.y-opy)
            };



            for(var j=0;j<this.opt.part;j++)
            {
                if(!gradeData[j])
                {
                    gradeData[j]=[];
                }
                if(!gradeData[j][i])
                {
                    gradeData[j][i]={};
                }
                gradeData[j][i].x=this.centerPosition.x+opx*(this.opt.part-j)/this.opt.part;
                gradeData[j][i].y=this.centerPosition.y-opy*(this.opt.part-j)/this.opt.part;
            }
        }
        this.gradePosition=gradeData;
        this.opt.data=data;
        this.pointPosition=pointData;

    };
    obj.draw=function () {
        this.drawChart();
        this.drawDiagonal();
        this.drawData();
    };
    //绘制对角线
    obj.drawDiagonal=function () {
        var ctx=this.can.getContext("2d");

        for(var i=0;i<this.opt.num;i++)
        {
            ctx.beginPath();
            ctx.moveTo(this.centerPosition.x,this.centerPosition.y);
            ctx.lineTo(this.pointPosition[i].x,this.pointPosition[i].y);
            ctx.lineWidth=this.opt.line_width;
            ctx.strokeStyle=this.opt.line_color;
            ctx.stroke();
            ctx.closePath();
        }
    };
    //绘制图形区域
    obj.drawChart=function () {
        var ctx=this.can.getContext("2d");
        var k=0;
        for(var i=0;i<this.opt.part;i++)
        {
            var bg_color=this.opt.color[k];
            var data=this.gradePosition[i];
            ctx.beginPath();
            ctx.moveTo(data[0].x,data[0].y);
            for(var j=0;j<this.opt.num;j++)
            {
                var next=(j+1)%this.opt.num;
                ctx.lineTo(data[next].x,data[next].y);
            }
            ctx.fillStyle=bg_color;
            ctx.fill();

            ctx.lineWidth=this.opt.line_width;
            ctx.strokeStyle=this.opt.line_color;
            ctx.stroke();
            ctx.closePath();
            k=(k+1)%this.opt.color.length;
        }
    };
    //绘制数据区域
    obj.drawData=function () {
        //绘制数据区域
        var data=this.opt.data;
        var bg_color=this.opt.chart_bg_color;
        var line_color=this.opt.chart_line_color;
        var line_width=this.opt.chart_line_width;

        var ctx=this.can.getContext("2d");
        ctx.globalAlpha=this.opt.chart_global_alpha;
        ctx.beginPath();
        ctx.moveTo(data[0].x,data[0].y);
        for(var i=0;i<this.opt.num;i++)
        {
            var next=(i+1)%this.opt.num;
            ctx.lineTo(data[next].x,data[next].y);
        }

        ctx.fillStyle=bg_color;
        ctx.fill();
        ctx.lineWidth=line_width;
        ctx.strokeStyle=line_color;
        ctx.stroke();
        ctx.closePath();

        ctx.beginPath();
        ctx.globalAlpha=1;
        ctx.fillStyle="black";
        ctx.font=this.opt.chart_title_font;
        for(var j=0;j<this.opt.num;j++)
        {
            ctx.fillText(data[j].name,this.pointPosition[j].fontx,this.pointPosition[j].fonty);
        }
        ctx.closePath();
    };
    obj.clearCan=function () {
        var cxt=this.can.getContext("2d");
        this.can.height=this.can.height;
    };
    obj.setOpt=function (opt) {
        Object.assign(this.opt,opt);
    };
    obj.init=function () {
        if(this.opt.num<3)
        {
            alert("图形边数不能小于3");
            return ;
        }
        if(this.opt.data.length<this.opt.num)
        {
            alert("数据量不能少于"+this.opt.num+"条");
            return ;
        }
        this.can=document.getElementById(this.opt.el);//获取canvas对象
        this.clearCan();
        this.getLen();
        this.getCenter();
        this.getOtherP();
        this.draw();//开始绘制
    };
    return obj;
}